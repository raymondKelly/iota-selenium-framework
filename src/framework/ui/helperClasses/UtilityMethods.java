package framework.ui.helperClasses;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Random;

public class UtilityMethods {

	public static String getOperatingSystem() {
		return (System.getProperty("os.name").toLowerCase().contains("mac") ? "mac" : "windows");
	}

	/***
	 * Example return format 2018-12-09T11:00:45.457
	 * 
	 * @return
	 */
	public static String getTimeNow() {
		return LocalDateTime.now().toString();
	}

	/***
	 * get current date and time in specific format, e.g. "dd.MM.yyyy" or
	 * "MM.dd.yyyy hh:mm"
	 * 
	 * @param timeFormat
	 * @return
	 */
	public static String getDateAndTime(String timeFormat) {
		DateFormat dateFormat = new SimpleDateFormat(timeFormat);
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime());
	}

	public static int generateRandomIntInRange(int minValue, int maxValue) {
		return (int) (Math.round(Math.random() * (maxValue - minValue))) + minValue;
	}

	/**
	 * Returns a pseudorandom, uniformly distributed integer value between 0
	 * (inclusive) and the specified value (exclusive)
	 * 
	 * @param maxValue
	 *            exclusive maximum value
	 * @return Pseudorandom integer
	 */
	public static Integer generateRandomInteger(Integer maxValue) {
		Random randomGenerator = new Random();
		return randomGenerator.nextInt(maxValue);
	}

}
